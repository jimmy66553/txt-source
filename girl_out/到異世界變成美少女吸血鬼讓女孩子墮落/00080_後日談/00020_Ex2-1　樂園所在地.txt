那裡是，地上最後的樂園。

在所有的国家都被吸血鬼攻陷之後，倖存下來的為數不多的人類，為尋找能作為人類活下去的地方而彷徨著。
然後，平安無事逃脫了的人們，偶然間到達了同一個地方。

位於大陸北部，通過洞穴後的，一片被山包圍的平原。
那裡好像曾經存在一個村落，數十人悄悄地在那裡生活過。

但是年輕人都去了大城市，剩下的人類也都死去了，只剩下腐朽的木造廢墟。
有的人是為了尋求藏身的地方，有的人是為了避雨，有的人是為了找到一個死去的地方，最後都到達了那裡。

村落的傳聞不知不覺傳播到了倖存下來的人們那裡，人們開始聚集在了那個地方。
幸運的是，土地相當寬廣，即使是聚集了近兩百人的現在，也沒有因位置而發生爭執。
只要堵住洞穴，就不用擔心會被吸血鬼發現的安全場所，不久後，在那裡生活的人們便開始稱之為「伊甸園」

伊甸園曾經很和平。
對暴露於生存危機下的人類，比起任何時候都要尊重自己的生命，還有他人的生命。
無論是誰，都會體諒他人，給予他人溫柔。

正可謂是理想鄉。
人類要是能拋棄愚蠢，也就不會一直爭鬥不止。
有了希望。
即是光。
如果產生了光，就會產生影子。
和平沒有持續很長時間。

爭鬥的原因是，食物。
雖然可以用泉水來填補水資源，但是僅靠採集山草和狩獵野生動物，是無法養活兩百個人的。
飢餓摧殘了人類的心靈，削去了溫柔。

畢竟，人類還是沒辦法創建出樂園的。
最初只是友人間食物的互相補助。
可是，不久後就形成了巨大的派系，產生了貧富差距，捲入了嫉妒的漩渦，派系之間的爭鬥開始了。

即便世界變成了只有兩百人的村落，也只是規模變小了，要做的事情也沒有什麼變化。
以血還血的戰鬥持續了數日，伊甸園的人口減少到了一百五十人左右。

── 那一天，一個人出現在了村落裡。

宛如神一般從天上降臨的他，名為亞歷山大・奧密克朗。
在人類還是世界霸者的時候，作為「世界上最優秀的魔法使」而馳名，是貨真價實的一流魔法學者。
他在目睹了世界被吸血鬼毀滅之後，尋找著可以隱居的地方，到達了伊甸園。

『著名魔法使的話，也許會知道能夠拯救我們的方法』

疲於爭鬥的人們依靠了亞歷山大。
他稍微想了一下，觀察了周圍之後，這樣說道。

「參加戰鬥的都是成年男性，也就是說，死者也都是男性。剩下的倖存者，如果男性是五十人，那麼女性就是一百人，不認為太多了嗎？」

在男性當中，似乎也有人在抱怨，為什麼就只有自己非得要戰鬥不可。
對於亞歷山大的話，很多人表示了贊同。

「應該保持平衡。同時也有了解決食物問題的方法，請按我說的去做」

那麼，亞歷山大確實是個優秀的魔法使，但實際上，在人類世界滅亡前不久，在混亂中很少有人知道，他已經被他自己的国家驅逐出境了。
表面上說他的擅長領域是治療傷病，但那是不對的。

── 人體實驗。

特別是針對於年輕女性，為了滿足自己的性欲，不擇手段，進行了各種淒慘的實驗。
這才是亞歷山大最擅長的領域。

亞歷山大這樣提議道。

「把五十個女人作為食物，剩下五十個女人用來作為高效率生產肉類的工廠。生孩子只需要幾個女人就足夠了」

最初，誰都是反對的。
可是，人類無法抵抗食欲。
從倖存者中選出了五十名女性，用來作為食物而被消費了。

剩下的五十名女人被捆綁著，通過亞歷山大的手術改造了子宮，妊娠的話，就能立刻產下巨大的肉塊。
很明顯，那些生產行為也是為了發泄男性們的性欲。
曾經吃過一次人肉的男性們，已經不能再把女性當成同一種生物來對待了。
順從的奴隷，方便和好用的道具，美味的食物。

食欲，性欲，睡眠欲，全部都被滿足了的男性們，與亞歷山大一起圍著飯桌，由衷的快樂地說道。

『這裡果然就是樂園』


◇◇◇

「歡迎回來，千草大人」

不知道為何，莉娜在等著從門那邊回來的我。
也沒聯絡過什麼時候會回來，難道是一直在這裡等著嗎。

「莉娜⋯⋯呃，我回來，了。特意這麼來迎接我，是發生了什麼事情嗎？」
「嗯，大概」

這又是一個，含糊不清的回覆呢。

「因為我是蕾雅的代理，所以不太了解具體情況」
「那她本人怎麼了？」
「被阿謝拉拽住了，在談話中」
「啊啊⋯⋯」

大概是，又想到了什麼新的Play了吧。
那三人真積極啊，該說是惹人欣慰嗎。

「所以，在蕾雅回來之前，和我說說話好嗎？」
「可以喲。話說，瑪麗怎麼樣了？」

瑪麗指的是，莉娜所生下的女兒。
現在是出生六個月。
紫色的頭髮，還有容貌，比起莉娜更像蕾雅。
最近能走路了，稍微不注意就會不知道跑到哪裡去了，很讓莉娜困擾。
順便一提，蕾雅的女兒取名為芙斯，髮色和容貌都和莉娜很相似。
兩人以會讓人嗤笑的程度溺愛著愛女，而且她們之間的關係也好得，不會輸給莉娜和蕾雅那般。
覺得城堡裡的氣氛比起之前更加開朗了，這恐怕不會是心理作用吧。

「瑪麗在拉萊拉萊那裡喲」
「還真是疼愛吶」
「從以前開始就很喜歡小孩子，被稱為聖母可不是徒有其名的」

雖然以前是錯誤意義上的聖母，但最近好像變得正確了。
嘛，和阿謝拉，還有姫莉進行性行為的時候，還是依舊能從房間裡聽到那野獸般的喘息聲。

「本人好像也懷孕了吧？」
「嗯，是阿謝拉的孩子。然後，阿謝拉懷著 都 的孩子，姫莉則是懷著拉萊拉萊的孩子」
「大體上是同一時期的，要變得更吵鬧起來了」
「一不注意，就會到處都是孩子了。不改造謁見室用來給孩子使用的話，恐怕房間都會不夠用」

莉娜開玩笑似的說道。
但是，這未必就是玩笑話。

一邊想像著在不久的將來，王城成為了托兒所的情景，一邊想著 ── 是不是要從那邊的世界裡買來育兒道具比較好呢。


◇◇◇

我進入了莉娜和蕾雅平時生活的房間，坐在了沙發上。
莉娜坐在了我的身旁，身體靠著這邊。

「像這樣和莉娜兩個人獨處，還真是少有啊」
「嘛，因為我總是和蕾雅在一起。不過，偶爾這樣也很不錯。像這樣觸摸著千草大人，心情就會平靜下來」

莉娜好像意味的喜歡我。
面對面，或者閑聊的時候，經常會像這樣靠到我身上觸摸我的身體。

「也許，千草大人會認為我已經忘記了。其實，我有好好記得」
「是什麼？」
「在我的身體還沒恢復的時候，愛著只是肉穴的我」

啊啊 ── 原來如此。

無法正常對話，也看不見，作為道具而被使用的莉娜。
確實如她所說，我擁抱著，愛著那種狀態下的她。
居然還記得當時的事情，沒想到現在的好意正是因為記得那時候的事情。

「啊，那個表情，果然是忘了吧。不過算了，我只是想隨便感恩，隨便返還給你罷了」

這樣說著，莉娜更加緊貼著我的身體，把臉靠了過來。
被如此誘惑了，就不能不作出回應了。

「真讓人開心，你的那個心意」

我把手放在她的下巴上，向上抬起，讓嘴唇重疊了過去。
與柔軟的櫻花色互相觸碰，再慢慢分離後，莉娜用濕潤了的眼睛看著這邊。

「吶千草大人。我啊，這次⋯⋯想要，千草大人的孩子」

莉娜一邊說著，一邊握著我的手，放到了肚臍附近。
我稍微往指尖上用力，陷入到了緊致而又柔軟的肚子裡，「én」，她發出了些許微弱的聲音。

「⋯⋯不行？」
「莉娜 ──」

⋯⋯如果你願意的話。

想就這麼說的時候，我停止了言語。
那是不行的。
被渴求的是，積極渴求的言語。
那種被動的言語，是無法讓莉娜幸福的。

「⋯⋯我想讓莉娜懷孕，想要你生下我的孩子」
「這，這樣啊，誒嘿嘿⋯⋯想要生下，和我的，孩子。那麼，現在就⋯⋯」

莉娜陶醉了似的綻放出了笑容，正想要進行一次深吻 ── 就在這時，響起了嘎吱的聲音。
我們停止了接近，同時把視線轉向了門的方向。

「我回來了莉娜，千草大人回來了⋯⋯」

接著，和進入了房間的蕾雅正好對視著。
這是什麼吶，這種，像是出軌暴露了一樣的罪惡感。

「我，打攪到你們了⋯⋯吧」
「不，完全沒有！蕾雅也過來啊，是說過想要千草大人做些什麼吧」
「是這樣的嗎？」
「啊，嗯⋯⋯嗯，嘛，是，的」

莉娜招著手，蕾雅很客氣的，扭扭捏捏的向著這邊走來。
不過從莉娜知道蕾雅的欲求這一點來看，兩人獨處的時候也在說著關於我的話吧。
在自己不知道的地方說著關於自己的話，稍微有點害羞了起來。
該說是瘙癢感，還是高興呢。

「打擾，了」

然後，蕾雅也坐在了我的旁邊，和莉娜一樣靠了過來。

「蕾雅也想對千草大人表示感謝。能與我再會，獲得自由，這些全都多虧了千草大人。對吧，蕾雅？」
「⋯⋯」

蕾雅就那麼無言的，紅著臉，點了下頭。

「但，是。在那之前，我就和千草大人做過了呢」
「要被嫉妒了哦？」
「不要緊的，馬上就會給蕾雅做的」

不知不覺間被預約了。
嘛，反正從一開始就有那個打算。

然後，我靠近了莉娜的嘴唇，互相纏繞著舌頭，開始了緩慢的接吻。
在蕾雅這個伴侶的眼前，以融化了似的表情，「Pú Qiú Róu，Jiú Róu，Qiú Báa」，和伴侶以外的某人接著吻。
大概是這種狀況增幅了快感，只是撫摸著莉娜那特別軟弱的舌根，她就，「én Yáa，Hiú」，喘著息。
在持續了數分鐘的充分的唾液交換後，我離開了莉娜的嘴唇，暫時凝視著她。

像是夫婦間的親密交流。
用只有她才能聽見的微小聲音，一邊撫摸著大腿，後背，腦袋，耳朵，一邊低聲細語著，「愛你」，「真可愛」，「想做更多」，這樣的愛的言語。

當冷靜下來後，接著把蕾雅拉了過來，稍微有些強硬的把嘴唇重疊了上去。
對於伴侶就在眼前的這一狀況，還是蕾雅這邊的抵抗感更強一些，最初是全身緊張得都僵硬著。

為了讓她放鬆下來，仔細地在她地口中愛撫著。
大概是口中殘留的莉娜的味道讓她感覺到了安心，一點點地緩解了蕾雅的緊張。
接著，完全沉迷於和我交合的她，每當吞下我送來的唾液是，就會發出著，「én Kú，én Fú úu」，的聲音，顫抖著身體。

莉娜垂涎欲滴似的在近處注視著，我和蕾雅的接吻。
她的眼神簡直就像是等待著獵物的野生動物一樣，恐怕是打算在我離開的那一瞬間奪走蕾雅吧。
那個預感完美命中了 ── 連接著我與蕾雅的唾液之線斷掉之前，莉娜就開始貪享著蕾雅的嘴唇。
這次輪到我被涼在一邊了。
沒辦法，我決定先開始，蕾雅所希望的那個行為。

手指輕輕一動後，從房間的陰影中伸出了黑色觸手。
我操縱著那個，首先是纏繞在了蕾雅的腳上。
或許是親吻著的她也注意到了這一點，就那麼保持著合在一起的嘴唇，只是移動著視線，看向了腳下。

但是莉娜那強烈的求愛不允許這個。
大概是出於對轉移了注意的嫉妒吧，莉娜用雙手抓著蕾雅的臉頰，更加深深地插入了舌頭。

「én，én úuu，én Fú，Fú ú úu，Qiú，Qiú Pá，Há Pá⋯⋯Fú，Fú úu én⋯⋯」

那個過分熱情的愛撫，恍惚了蕾雅的表情。
在這一期間，影子已經完全包入了她的下全身。
我又逐步緊逼，讓影子爬了上去，到達了肚臍，胸，腋下，胳膊，脖子 ── 直到全身都被漆黑的影子所覆蓋。
影子溶化了衣服，緊緊的貼在了蕾雅的肌膚上。

「Há Hé，Hé éii⋯⋯Qiù Pù，Há⋯⋯來，來了⋯⋯ō⋯⋯這個，én Gú，Gó Líi⋯⋯！」

大概是這全身都被影子包入的感覺，讓蕾雅想起了和我戰鬥時的記憶，她的表情不像樣的恍惚著。
看到了那個的莉娜放開嘴唇，轉向我這邊說道。

「吶～千草大人，我也想要⋯⋯給我做這個」
「當然會給你做的，一開始就是這麼打算的」

影子爬在了莉娜身上，從她的腳開始，按順序一點點的被「黑色」所吞入。

「ā⋯⋯Hà，這個⋯⋯én Há áa！」

實際上，因為我也沒有親身體驗過，所以不太清楚。
不過，據說是用手指和舌頭觸摸所完全不一樣的感覺，一旦品嘗了一次就再也無法擺脫了。
當莉娜的脖子被覆蓋住時，她的呼吸聲就已經是斷斷續續的了，已經不能正常說話了。
但是還遠遠不止於此哦 ── 因為這還只是，外部。
爬上去的影子到達了嘴邊，滑滑的侵入了進去。
口蓋，牙齦，從舌根一直到內部的，所有地方都被蹂躪著，填滿了。

「á，á，Góo⋯⋯ó，én óoo ó！」

已經知道了那個快樂的蕾雅，期待的震動著聲音。
影子進入了更深處，不傷害喉嚨和食道粘膜的同時，從內部開始一邊擴張著，一邊前進著。

「ó óo ó，ó，Há ó óo ó ó！？」

就像是插入了一個粗大的管子一樣，莉娜緊繃著肚子，視線徘徊於虛空之中。
影子爬滿了臉上的所有部分，從鼻子，耳朵，還有眼睛侵入到體內。
然後，腦袋被完全包住了的兩人，已經不能再發出聲音，只是憑著被給予的感覺顫抖著身體。
我撫摸著被漆黑之影所包裹著的蕾雅的大腿。

緊接著，突然！她劇烈地向後仰起了身體。
我把舌頭放入了被染黑的，滑溜溜的莉娜的口內，攪拌著。
緊接著，這邊也是用全身的痙攣，來表達身體上的快樂。

在那之後的一段時間內，讓兩人躺在了地板上，坐在了沙發上，就那麼眺望著兩個翻來覆去的黑塊。
雖然這是單方面的，但是接受了這一切的兩人真的很可愛。
偶爾用腳，手，舌頭去觸摸的話，就會產生過剩反應的那個身姿真的很可愛。
永遠都不會感到厭倦，我讓身體發燒著，靜靜地觀察著兩人的身姿。


◇◇◇

說起來，雖然想起來也已經晚了。

蕾雅應該是有事情找我的，但是她已經昏過去了，在床上呼著可愛的睡息。
沒辦法了，直到她醒來為止，連同昏過去了的莉娜一起，看著兩人的睡臉，等待了三個小時左右。

她終於醒過來了。
代替了早安的問候親吻了額頭後，蕾雅不知是不是睡迷糊了，就像是孩子一樣緊緊地抱住我，把臉埋進了胸裡。

⋯⋯之後再問吧，看起來，她好像一直都是對莉娜這麼做的。

關係好到讓人羨慕，偶爾像今天這樣，讓我也沾了點，也不會有什麼報應吧。

「那麼蕾雅，找我有什麼事情嗎？」

抱著胸前的蕾雅的腦袋，這樣問著。
接著，她突然恢復了理性，保持了和我的距離，焦急的樣子說道。

「對，對了！那個，還存在著活著的人類。而且⋯⋯有幾個人想要把，那其中的女性，變為我們的同伴，而接近了過去」
「然後，因為某種理由而失敗了麼？」
「嗯。洞穴的對面，有一個村落⋯⋯好像是這樣的。那個洞穴前，有幾個女性。但是那些人⋯⋯大家都無法，魅惑」
「像姫莉一樣對魔法有耐性嗎？」
「交戰過的孩子們說⋯⋯人類所做不到的動作，擊打到的感觸也是非常的僵硬⋯⋯」

無法魅惑。
非人類的動作。
擊打到的觸感非常僵硬。

從這些情報中，突然浮現出來的是，人造人這個單詞。
但是，在這個世界上，不存在會被錯認成女性的人造人技術。

「有受傷人員嗎？」
「聽說有重傷者。包括這個在內，她們向千草大人尋求著幫助」
「的確，現在可不是睡覺的時候」
「我可不是⋯⋯想睡覺才睡的」

蕾雅不快的盯著我。
嘛，就像是被莉娜和我捲入了一樣。
剛才的說法，是不是有點欺負人了。

「說不定需要借助於蕾雅的知識，在莉娜起來後，三人一起去那裡吧」
「嗯，我明白了」

就這樣，我決定了，和莉娜，蕾雅一起，前往人類最後的樂園。

⋯⋯也就是說，不得不拜託艾莉絲和都姐姐留守在家。

剛回來，沒說幾句話就又外出了，大概會因此而鬧別扭吧，那兩人。