在和號碼們的戰鬥中取得勝利並奪取他們，意氣風發的夏爾醬們將朝著和魔神的最終決戰而努力，這是上一次為止的超簡樸的故事。

在安靜的湖畔的圓桌上，總是聚集著許多面孔。
但是和平時的風格不同。

有很多的梅傑德大人。

用白布覆蓋全身，只在眼睛處露出來，額頭上分別寫著喜歡的數字。
話說『７』還戴了三個，那是……。

不管怎麼說，只有我是一個黑壓壓的人，真讓人掃興。

「那麼新生卡美洛――因為這次有遠程的參加者，所以開始對外的臨時名稱『克洛諾斯』的圓桌會議。」

額頭上寫著『１』的夏爾莊嚴地告知。

正如我妹妹所說，這次是用濕婆的通信用結界邀請一個外人的聚會。

在漂浮在虛空中的畫面上，『１』的人阿列克謝・古貝爾克前輩戴著頭巾。

因為是第一次，所以其他號碼的人都不參加，只有一個人代表參加。

『有特意隱藏真面目的意思嗎？不，怎麼看都不像人的人映在通信魔法道具的一端，所以想隱藏的心情也不是不能理解……』

知道是基根。
就像是巨大佛像的揭幕儀式前一樣。

「外形很重要。我覺得不能忽視樣式美。」

「雖然【號碼】們是【大家都應該平等】的理念下用數字互相稱呼的，但我覺得你們有明確的序列。蒙面，用數字……或者說是因為『７』戴著所以真的沒有意義嗎？」

你果然注意到了那裡嗎？不愧是阿列克謝前輩。

「總之！就是！我們和號碼們的各位，就好像在河灘上以夕陽為背景互相毆打之後，萌發了純潔美麗的友情，開始攜手合作。一起！為了戰勝巨大的罪惡！」

『……』

阿列克謝前輩表情複雜，令人吃驚。蒙面也知道。

「那麼，巨大的惡是什麼？沒錯，是企圖復活魔神的邪惡的巨大組織！雖然我覺得是這樣，但是情況好像更嚴重了呢。」

「這麼說來？」

梅傑德號碼『１３』的人問。

這個『時間之神(克洛諾斯)怎麼樣？24小時表示？』做出這樣奇怪選擇的是身為奇怪女子的提亞教授。

「在我們對決的場合出現了兩個魔人。這個魔神已經復活了吧？」

夏爾醬還是一如既往地直指問題所在。

「如果魔神復活的話，不直接去嗎？」

輕描淡寫地告訴我的是梅傑德『４』，麗莎。

「因為最終頭目是坐著等著的，所以才是最終頭目。本來抱著神字的人就很難直接下手。另外，從孕育出兩個擁有如此強大力量的家庭開始，我認為現在的狀況是可以大大地行使這種惡勢力的。」

有幾個人點頭說「原來如此」。

『……』

阿列克謝前輩好像想說什麼，但是沒說。
為什麼呢，因為我們已經達成了事先和我協商閉口的協定。

我不喜歡被魔神般的東西附身的他，所以接觸了他問了很多。

原來如此，嚇了我一跳。
據說魔神路西法拉的本體，是被奪取王妃吉澤洛特的身體。

雖然還不完全，但今後可能會做很多壞事。
另一方面，給吉澤洛特戴上項圈，警惕著至今為止一直打擾我的我。

如果對夏爾說『聽說王妃變成魔神了』的話，會怎麼樣呢？

「不愧是兄長大人啊。就算我們什麼都不做也一定會解決的。」（吹棒）

誰想看到我的天使眼中失去亮點？ 

這次我也想讓夏爾徹底享受黑子的樂趣。

因此，我考慮了很多，是的。

額頭上有個『０』的梅傑德大人。

「既然敵人已經確定了，就不要客氣。馬上給他打個招呼吧！」

「但是，魔神還在哪裡，他的樣子也不清楚。」

對於夏爾的話，『10』的人――伊利斯認真地說道。

「我們不得不落後。必須一邊處理來自那邊的動作，一邊收集情報。」

「我做不了這麼麻煩的事」，『７』的一人，萊斯說道。

「那邊明確地瞄準了我們。經常保持警戒也只是疲敝而已……」

我不知道瑪麗安公主選擇『８』的理由。其他成員也是這樣。

「正義的夥伴就是這樣的。一邊擊潰那邊的一個小把戲，一邊和中頭目苦戰，一邊提升力量，最後將頭目消滅掉。」

動畫或者特攝英雄劇就是這樣的吧。幾集結束的話會很糟糕的。

只是現實是無情的。
魔神竟然會認真遵守那樣的『約定』………嗯？

我已經知道敵人的來歷了。

而且是被我掌握著生殺予奪的權利的吉澤洛特。

只要打個招呼用釘子釘一下，就可以按照我的想法進行了吧？

不遵守的話，讓他保護不就好了嗎？那種感覺。

啊，對方是魔神之類的神一般的東西。一定很強。但是應該還沒有完全復活（樂觀的預測）。

即使最終頭目是吉澤洛特，也有點問題。

首先我應該確認一下，去了邊境伯的城堡――義父那裡――。 