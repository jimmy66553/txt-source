「來得正好吶，冒險者艾麗婭與其同伴們喲」
「素、素的！」


跪下來的艾麗婭發出了類似悲鳴的回應。
因為太過緊張她的身體也顯得十分僵硬。


這也是無可厚非的。
畢竟眼前的這位，是這個國家的皇帝，名為「蓋澤魯・奧修拉」的存在。

雖然對他旁邊那皇妃的座位空著的事情感到在意，但艾麗婭已經顧不上這些了。


「不用那麼緊張吶，你們是拯救了格拉德斯通的英雄啊，堂堂正正地挺起胸膛便可。接著……那位是史黛拉，而那邊的元素貓就是塔瑪了吧」
「正是如此！的說！能一睹皇帝陛下之風采乃是吾之光榮！的說！」
「喵！」


皇帝說完後，史黛拉和塔瑪都精神飽滿地回應。
而史黛拉這種奇怪的語調，是經過艾麗婭給她惡補言語上的禮儀之後臨陣磨槍的成果……不過和之前那種隨意的語調比起來確實變得有禮節多了。


「恩呣，這次關於守護格拉德斯通的事你們做得很好，感謝你們。之後會由國家授予你們褒獎」

對皇帝的話，艾麗婭深深地低下了頭。而薇兒卡和史黛拉，還有跟著一起來的莉莉和菲莉也同樣。


「話說回來……真是好久不見了吶，薇兒卡喲」
「是，久疏問候喵。陛下貴體安康勝作萬事喵！」


皇帝十分唐突地向薇兒卡說道。
薇兒卡則既精神又恭敬地回應。


對著突如其來的事，艾麗婭和塔瑪都一臉「「………………！？」」驚訝的樣子。
到底是怎麼回事呢。為何這位一個人經營著武器店的店主薇兒卡，和皇帝陛下好像以前就認識一樣在交談呢？


「這是在做什麼啊薇兒卡喲，因為在同伴面前所以要用那種地位的語氣來跟朕說話？這次叫你過來明明還有另外一個理由的……」
「唔喵……果然，把我也叫過來是為了那件事喵……？」
「恩呣，對想過那種樂無憂的開武器店生活的你來說雖然有點抱歉……嘛，接下來的事還是讓朕的兒子來說明為好。——進來吧，「朱利烏斯」!」
「——謹遵王令，陛下」


與皇帝的話相應，王座旁邊的門被打開了。


現身的是一位青年。
身高大概超過一米八，即便身穿白銀色的鎧甲，也能看得出那經過千錘百鍊的體格。
天藍色的長髮蓋過了脖子，那銳利的、同時也相當知性的眼睛也如頭髮般亦是天藍色。


他的名字叫做「朱利烏斯・奧修拉」——這個國家的第一皇子。


(朱利烏斯……！就是那位曾經與大魔法使一起，在魔神的黃昏裡活躍的勇者皇子嗎！)


聽到他的名字後，塔瑪用充滿尊敬的眼神看向朱利烏斯皇子。
對……他就是這個國家的第一皇子，同時也是曾與大魔法使一起拯救了世界的勇者之一。

「這麼說來，還真是很久不見了吶，「神器鍛造師・薇兒卡」」
「「…………！？」」

艾麗婭和塔瑪再次嚇得倒吸一口氣。

(神、神器鍛造師？薇兒卡桑她……！？)
(竟然！之前還以為想多了，沒想到還真是……！)

艾麗婭心裡十分驚訝。
而塔瑪在那次庭院裡的世界樹事件中察覺到薇兒卡的慌亂，那時就猜到了這個可能性……不過果然還是很驚訝。


「喵啊……既然光明正大地暴露我的身份出來，那就說明又要製作神器了喵，對吧殿下……？」
「正是如此，薇兒卡。還有艾麗婭，對你們也有委託」
「是……是滴！」


薇兒卡是神器鍛造師——
被這個頗具衝擊性的事實驚訝得還半張著嘴的艾麗婭連忙回話。
「艾麗婭，還有你的夥伴們……要不要加入我率領的「帝國勇者團」？」
「帝、帝國勇者團，是「那個」帝國勇者團嗎！？而、而且讓我加入……？」


帝國勇者團——那是朱利烏斯皇子親自率領的由勇者和戰鬥專家組成的帝國最強戰力。
魔物泛濫、暗謀著的魔族集團、國家級別危險程度魔物的出現等等，這些就是使用那份力量的時候。

而剛才朱利烏斯皇子問要不要加入的，正是這個帝國勇者團。


「吔蔗糖？那是何物，好吃嗎？的說！」


無法理解那個單詞的意義的史黛拉，立馬就把心裡想到的東西說了出來。


「這位……應該就是史黛拉了吧？簡單來說，就是要不要加入一個能跟強大的魔物戰鬥的隊伍」
「強敵！艾麗婭，吾要加入此吔蔗糖！」
「等、史黛拉醬……！」


對本能上尋求著與強敵交手的史黛拉來說，朱利烏斯皇子這種簡單過頭的說明卻具有無比的吸引力。
而對這二話不說就要入團的史黛拉，艾麗婭慌亂了起來。


「朱利烏斯殿下，為什麼要招募艾麗婭她們喵？確實艾麗婭醬已經變強了，但實力也尚在提升的過程中喵」
「當然那是有理由的，薇兒卡。雖然這還尚未公開於眾——」
「朱利烏斯皇子，那裡就讓我來說明」


一道聲音打斷了朱利烏斯皇子的話。


到底是什麼時候來的呢。
他的身旁，有一名少女——不，幼女出現了。


金瞳灰髮，幼小的身軀被漆黑色的哥特連衣裙包裹著，真是一絕世美幼女。
年幼的臉龐上浮現的，是妖艷的……而又帶有些許頹廢般的笑容。


「……別突然出現啊，「貝露澤碧尤特」。嘛，那就拜託你好好說明了 」
「呵呵，明白了喲，皇子大人」


在幼女突然出現後，朱利烏斯皇子仿彿也有點無語地說道。
而這位幼女則帶著惡作劇得逞的笑容回話……。


在一旁看著的艾麗婭和塔瑪已經口瞪目呆了。


(貝、貝露澤碧尤特……難道是，那位「聖魔王貝露澤碧尤特」！？)


艾麗婭和塔瑪的內心裡都驚訝不止。


而正在此時——

「呵呵，就是那樣子喲，精靈族的小姐。我的名字乃貝露澤碧尤特，正是你們崇拜的被稱作聖魔王的存在哦」


——幼女……不，聖魔王貝露澤碧尤特向艾麗婭作出了說明。

「…………！？」


艾麗婭大混亂中。
明明剛才自己什麼都沒有說。
可是為何眼前的這位幼女卻像看穿了自己內心的想法一般，說出了那樣的話呢。


「唔喵，貝露澤碧尤特大人，總是那樣嚇唬人我覺得不太好喵」
「呵呵……也沒什麼不好的嘛，薇兒卡。我能讀心的事，大家應該都知道的吧……？」


聽到薇兒卡帶著苦笑般的提醒，貝露澤碧尤特臉上再次露出了惡作劇得逞的笑容。

(讀心能力……傳說原來是真的嗎！)

聽著她們的對話，塔瑪明白了。
眼前的這位幼女，的確是聖魔王貝露澤碧尤特這件事。


聖魔王貝露澤碧尤特——


曾經，有一個想要毀滅世界的魔神……其下有著被稱作七大魔王的存在。
其中，有一位成為了大魔法使的同伴並將力量借與他的正義魔王。
而那位魔王便是這位聖魔王。


傳說中，貝露澤碧尤特有著能讀懂人內心想法的能力。
塔瑪也知道這個傳說。


艾麗婭變得驚慌失措起來。
正在考慮是否要加入帝國勇者團的時候，突然蹦出來一個聖魔王，而那位聖魔王正跟薇兒卡好像很熟絡般地交談著。
她的思考迴路已經進入過載狀態了。


「嗯喵，冷靜下來艾麗婭醬。在貝露澤碧尤特大人說明之前，我會先跟你解釋各種各樣的事喵」


看到艾麗婭這個樣子，薇兒卡出言道。
皇帝、朱利烏斯皇子還有貝露澤碧尤特都點了點頭後，薇兒卡便開始了說明。


「首先，實際上我是神器鍛造師喵。然後，關於為什麼我能不受國家的束縛開了一家武器店這件事……那是我和大魔法使——舞夜醬在魔神的黃昏裡並肩作戰過後得到的恩賜，在不擅自製造神器的條件下獲得了皇帝陛下的許可喵」
「薇、薇兒卡桑……和大魔法使，在魔神的黃昏裡並肩作戰……？」
「那個喵，本來不想說出來的，但這個狀況下我也不得不說了喵。平時作為冒險者雖然只有C級左右的實力，但我的固有技能的效果是裝備上神器後，能發揮出A級左右的力量喵」
「竟然……！？」

薇兒卡的過去，還有她隱藏起來的力量，讓艾麗婭驚訝連連。


在她腳邊的塔瑪在內心裡(原來如此……)地想著。
從以前開始，薇兒卡就和騎士隊長的塞德里克和大魔法使的舊相識很熟絡的樣子。
一介武器鍛造師怎麼會……塔瑪曾經這樣猜疑過，現在知道了薇兒卡有著這等背景後便釋懷了。


「這、這麼說的話……大魔法使的隊伍裡，好像有一位使用戰錘的獸人……「猛獸姬」大人來著……」
「那說的是我喵」


聽到艾麗婭的問話，薇兒卡苦笑著回答。


(呵呵，真是個可愛的孩子呢。只這種事就被驚訝成這個樣子，之後要怎麼才能進入正題好呢？)


看著被接連的各種事情驚訝到宕機的艾麗婭，貝露澤碧尤特臉上浮現出感到很有趣的笑容——
